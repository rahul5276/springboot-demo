**Springboot-demo project**

##What you’ll need
1. MySQL version 5.6 or better. If you have docker installed it might be useful to run the database as a container.
2. A favorite text editor or IDE
3. JDK 1.8 or later
4. Maven 3.2+

---

## Build the project

1. Go to 'src\main\resources\application.properties' and edit the mysql params to 
confirm to your local environment.
2. If you’re not familiar with Maven, refer to <a href="https://spring.io/guides/gs/maven/">Building Java Projects with Maven</a>.
3. You can build the JAR file with ./mvnw clean package. 
4. Then you can run the JAR file:
   java -jar target/springboot-demo-0.1.0.jar

## Test the application

You can run the application, and the REST services would be available at the following context.
*localhost:8080*.

## API 
http://localhost:8080/swagger-ui.html