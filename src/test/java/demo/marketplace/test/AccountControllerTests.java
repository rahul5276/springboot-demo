/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package demo.marketplace.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import demo.marketplace.repositories.Account;
import demo.marketplace.repositories.AccountRepository;
import demo.marketplace.repositories.Contact;
import demo.marketplace.repositories.ContactRepository;
import demo.marketplace.vo.AccountVO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AccountControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ContactRepository contactRepository;

    static boolean setupExists;

    @Before
    public void preTestDataSetup(){
        if(!setupExists){
            setupAccountData();
            setupExists = true;
        }
    }

    private void setupAccountData() {
        Account account = new Account();
        account.setAddressLine1("Humpty lane");
        account.setCity("Austin");
        account = accountRepository.save(account);
        System.out.println("account id:"+account.getId());

        Contact contact = new Contact();
        contact.setName("contact1");
        contactRepository.save(contact);
        Contact contact2 = new Contact();
        contact.setName("contact2");
        contactRepository.save(contact);
    }

    @Test
    public void getAllAccountsTest() throws Exception {
        this.mockMvc.perform(get("/api/accounts")).andDo(print()).andExpect(status().isOk());
    }
    @Test
    public void getAccountById() throws Exception {

        this.mockMvc.perform(get("/api/accounts/1")).andDo(print()).andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(1));
    }
    @Test
    public void createAccountTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        AccountVO accountVO = new AccountVO();
        accountVO.setAddressLine1("10 Baker Street");
        accountVO.setCity("London");
        accountVO.setPostalCode("390428");
        this.mockMvc.perform(post("/api/accounts/").contentType(MediaType.APPLICATION_JSON).
                content(mapper.writeValueAsString(accountVO))).andDo(print()).andExpect(status().isCreated());
    }

    @Test
    public void updateAccountTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        AccountVO accountVO = new AccountVO();
        accountVO.setAddressLine1("221B Baker Street");
        accountVO.setCity("London");
        accountVO.setPostalCode("390428");
        this.mockMvc.perform(put("/api/accounts/1").contentType(MediaType.APPLICATION_JSON).
                content(mapper.writeValueAsString(accountVO))).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void getAccountContactsTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        AccountVO accountVO = new AccountVO();
        accountVO.setAddressLine1("221B Baker Street");
        accountVO.setCity("London");
        accountVO.setPostalCode("390428");
        this.mockMvc.perform(get("/api/accounts/1/contacts")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void associateContactsTest() throws Exception{
        ObjectMapper mapper = new ObjectMapper();
        Integer contactId = 2;
        this.mockMvc.perform(put("/api/accounts/1/contacts").contentType(MediaType.APPLICATION_JSON).
                content(mapper.writeValueAsString(contactId))).andDo(print()).andExpect(status().isCreated());
    }


}
