/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package demo.marketplace.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import demo.marketplace.repositories.Account;
import demo.marketplace.repositories.AccountRepository;
import demo.marketplace.repositories.Contact;
import demo.marketplace.repositories.ContactRepository;
import demo.marketplace.vo.AccountVO;
import demo.marketplace.vo.ContactVO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ContactControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ContactRepository contactRepository;

    static boolean setupExists;
    @Before
    public void preTestDataSetup(){
        if(!setupExists){
            setupContactData();
            setupExists = true;
        }
    }

    private void setupContactData() {
        Contact contact = new Contact();
        contact.setName("contact1");
        contactRepository.save(contact);
    }


    @Test
    public void getAllContactsTest() throws Exception {
        this.mockMvc.perform(get("/api/contacts")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void getContactByIdTest() throws Exception {
        int id = contactRepository.findAll().iterator().next().getId();
        this.mockMvc.perform(get("/api/contacts/"+id)).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void createContactTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ContactVO contactVO = new ContactVO();
        contactVO.setAddressLine1("10 Baker Street");
        contactVO.setCity("London");
        contactVO.setPostalCode("390428");
        this.mockMvc.perform(post("/api/contacts/").contentType(MediaType.APPLICATION_JSON).
                content(mapper.writeValueAsString(contactVO))).andDo(print()).andExpect(status().isCreated());
    }

    @Test
    public void updateContactTest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ContactVO contactVO = new ContactVO();
        contactVO.setAddressLine1("221B Baker Street");
        contactVO.setCity("London");
        contactVO.setPostalCode("390428");
        int id = contactRepository.findAll().iterator().next().getId();
        this.mockMvc.perform(put("/api/contacts/"+id).contentType(MediaType.APPLICATION_JSON).
                content(mapper.writeValueAsString(contactVO))).andDo(print()).andExpect(status().isOk());
    }

}
