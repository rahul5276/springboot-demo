package demo.marketplace.controllers;

import demo.marketplace.repositories.Account;
import demo.marketplace.repositories.AccountRepository;
import demo.marketplace.repositories.Contact;
import demo.marketplace.repositories.ContactRepository;
import demo.marketplace.vo.AccountVO;
import demo.marketplace.vo.ContactVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
@RequestMapping(path="/api")
public class ContactController {

	@Autowired
	private ContactRepository contactRepository;
	
	@GetMapping(path="/contacts")
	public @ResponseBody Iterable<Contact> getAllContacts () {
		return contactRepository.findAll();
	}

	@GetMapping(path="/contacts/{contactId}")
	public ResponseEntity<Contact>  getContactById (@PathVariable("contactId") int contactId) {
		Contact contact = null;
		if(contactId <= 0){
			return new ResponseEntity<Contact>(contact, HttpStatus.BAD_REQUEST);
		}
		Optional<Contact> contactContainer = contactRepository.findById(contactId);
		if(!contactContainer.isPresent()){
			return new ResponseEntity<Contact>(contact, HttpStatus.NOT_FOUND);
		}
		contact = contactContainer.get();
		return new ResponseEntity<Contact>(contact, HttpStatus.OK);
	}

	@PostMapping(path="/contacts",consumes = "application/json")
	public ResponseEntity<String> createContact (@RequestBody ContactVO contactVO) {

		Contact contact = new Contact();
		BeanUtils.copyProperties(contactVO,contact);
		contactRepository.save(contact);
		return new ResponseEntity<String >("_CREATED",HttpStatus.CREATED);
	}

	@PutMapping(path="/contacts/{contactId}")
	public ResponseEntity<String> updateContact (@PathVariable("contactId") int contactId,
												 @RequestBody ContactVO contactVO) {
		if(contactId <= 0){
			return new ResponseEntity<String>("_BAD_REQUEST",HttpStatus.BAD_REQUEST);
		}
		Optional<Contact> contactContainer = contactRepository.findById(contactId);
		if(!contactContainer.isPresent()){
			return new ResponseEntity<String>("_NOT_FOUND", HttpStatus.NOT_FOUND);
		}
		Contact contact = contactContainer.get();
		BeanUtils.copyProperties(contactVO, contact);
		contactRepository.save(contact);
		return new ResponseEntity<String>("_UPDATED", HttpStatus.OK);
	}

}
