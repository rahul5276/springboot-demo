package demo.marketplace.controllers;

import demo.marketplace.repositories.Account;
import demo.marketplace.repositories.AccountRepository;
import demo.marketplace.repositories.Contact;
import demo.marketplace.repositories.ContactRepository;
import demo.marketplace.vo.AccountVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Optional;

@Controller
@RequestMapping(path="/api")
public class AccountController {

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private ContactRepository contactRepository;
	
	@GetMapping(path="/accounts")
	public @ResponseBody Iterable<Account> getAllAccounts () {
		return accountRepository.findAll();
	}

	@GetMapping(path="/accounts/{accountId}")
	public ResponseEntity<Account>  getAccountById (@PathVariable("accountId") int accountId) {
		Account account = null;
		if(accountId <= 0){
			return new ResponseEntity<Account>(account, HttpStatus.BAD_REQUEST);
		}
		Optional<Account> accountContainer = accountRepository.findById(accountId);
		if(!accountContainer.isPresent()){
			return new ResponseEntity<Account>(account, HttpStatus.NOT_FOUND);
		}
		account = accountContainer.get();
		return new ResponseEntity<Account>(account, HttpStatus.OK);
	}

	@PostMapping(path="/accounts")
	public ResponseEntity<String> createAccount (@RequestBody AccountVO accountVO) {
		Account account = new Account();
		BeanUtils.copyProperties(accountVO, account);
		accountRepository.save(account);
		return new ResponseEntity<String>("_CREATED", HttpStatus.CREATED);

	}

	@PutMapping(path="/accounts/{accountId}")
	public ResponseEntity<String> updateAccount (@PathVariable("accountId") int accountId,
												 @RequestBody AccountVO accountVO) {
		if(accountId <= 0){
			return new ResponseEntity<String>("_BAD_REQUEST",HttpStatus.BAD_REQUEST);
		}
		Optional<Account> accountContainer = accountRepository.findById(accountId);
		if(!accountContainer.isPresent()){
			return new ResponseEntity<String>("_NOT_FOUND", HttpStatus.NOT_FOUND);
		}
		Account account = accountContainer.get();
		BeanUtils.copyProperties(accountVO, account);
		accountRepository.save(account);
		return new ResponseEntity<String>("_UPDATED", HttpStatus.OK);
	}

	@GetMapping(path="/accounts/{accountId}/contacts")
	public ResponseEntity<java.util.Set<Contact>> getAccountContacts (@PathVariable Integer accountId) {
		java.util.Set<Contact> contacts = null;

		if(accountId <= 0){
			return new ResponseEntity<java.util.Set<Contact>>(contacts, HttpStatus.BAD_REQUEST);
		}
		Optional<Account> accountContainer = accountRepository.findById(accountId);
		if(!accountContainer.isPresent()){
			return new ResponseEntity<java.util.Set<Contact>>(contacts, HttpStatus.NOT_FOUND);
		}

		contacts = accountRepository.findById(accountId).get().getContacts();
		return new ResponseEntity<java.util.Set<Contact>>(contacts, HttpStatus.OK);

	}

	@PutMapping(path="/accounts/{accountId}/contacts")
	public ResponseEntity<String> associateContacts (@PathVariable Integer accountId,  @RequestBody Integer contactId) {
		Account account = null;
		Contact contact = null;
		String responseStr = null;
		if(accountId <= 0){
			return new ResponseEntity<String>("_IVALID_ACCOUNT_ID", HttpStatus.BAD_REQUEST);
		}
		if(contactId <= 0){
			return new ResponseEntity<String>("_IVALID_CONTACT_ID", HttpStatus.BAD_REQUEST);
		}
		Optional<Account> accountContainer = accountRepository.findById(accountId);
		if(!accountContainer.isPresent()){
			return new ResponseEntity<String>("_ACCOUNT_NOT_FOUND", HttpStatus.NOT_FOUND);
		}
		Optional<Contact> contactContainer = contactRepository.findById(contactId);
		if(!contactContainer.isPresent()){
			return new ResponseEntity<String>("_CONTACT_NOT_FOUND", HttpStatus.NOT_FOUND);
		}
		account = accountContainer.get();
		contact = contactContainer.get();
		account.getContacts().add(contact);
		accountRepository.save(account);
		return  new ResponseEntity<String>("_SAVED", HttpStatus.CREATED);
	}
}
